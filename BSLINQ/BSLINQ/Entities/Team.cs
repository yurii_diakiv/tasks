﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public Team(int id, string name, DateTime createdAt)
        {
            Id = id;
            Name = name;
            CreatedAt = createdAt;
        }

        public override string ToString()
        {
            return $"Id : {Id}\nName : {Name}\nCreatedAt : {CreatedAt}\n";
        }
    }
}
