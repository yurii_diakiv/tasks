﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }

        public Project(int id, string name, string description, DateTime createdAt, DateTime deadline, int? authorId, int? teamId)
        {
            Id = id;
            Name = name;
            Description = description;
            CreatedAt = createdAt;
            Deadline = deadline;
            AuthorId = authorId;
            TeamId = teamId;
        }

        public override string ToString()
        {
            return $"Id : {Id}\nName : {Name}\nDescription : {Description}\nCreatedAt : {CreatedAt}\nDeadline : {Deadline}\n" +
                $"AuthorId : {AuthorId}\nTeamId : {TeamId}\n";
        }
    }
}
