﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegistredAt { get; set; }
        public int? TeamId { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime birthday, DateTime registredAt, int? teamId)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Birthday = birthday;
            RegistredAt = registredAt;
            TeamId = teamId;
        }

        public override string ToString()
        {
            return $"Id : {Id}\nFirstName : {FirstName}\nLastName : {LastName}\nEmail : {Email}\nBirthday : {Birthday}\n" +
                $"RegistredAt : {RegistredAt}\nTeamId : {TeamId}\n";
        }
    }
}
