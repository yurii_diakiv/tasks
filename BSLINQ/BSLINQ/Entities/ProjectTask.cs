﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ.Entities
{
    public class ProjectTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        public int? ProjectId { get; set; }
        public int? PerformerId { get; set; }

        public ProjectTask(int id, string name, string description, DateTime createdAt, DateTime finishedAt, int state, int? projectId, int? performerId)
        {
            Id = id;
            Name = name;
            Description = description;
            CreatedAt = createdAt;
            FinishedAt = finishedAt;
            State = state;
            ProjectId = projectId;
            PerformerId = performerId;
        }

        public override string ToString()
        {
            return $"Id :{Id}\nName : {Name}\nDescription : {Description}\nCreatedAt : {CreatedAt}\nFinishedAt : {FinishedAt}\n" +
                $"State : {State}\nProjectId : {ProjectId}\nPerformerId : {PerformerId}\n";
        }
    }
}
