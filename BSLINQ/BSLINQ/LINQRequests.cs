﻿using System;
using System.Collections.Generic;
using System.Text;
using BSLINQ.Entities;
using System.Linq;
//using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
//using LinqKit;
//using System.Data.Entity;

namespace BSLINQ
{
    public class LINQRequests
    {
        public void FirstTask(int id, List<Project> projects, List<ProjectTask> tasks)
        {
            Console.WriteLine("---First task---");
            Console.WriteLine();

            var first = (from project in projects
                        join task in tasks on project.Id equals task.ProjectId
                        where task.PerformerId == id
                        group task by project into gTbP
                        select new { gTbP.Key, countOfTasks = gTbP.Count() });

            List<int> vs = new List<int>();
            foreach (var i in first)
            {
                Console.WriteLine("Project:");
                Console.WriteLine(i.Key);
                Console.Write("Count of tasks: ");
                Console.WriteLine(i.countOfTasks);
                Console.WriteLine();
            }
        }

        public void SecondTask(int id, List<ProjectTask> tasks)
        {
            Console.WriteLine("---Second task---");
            Console.WriteLine();

            var second = tasks.Where(t => t.PerformerId == id && t.Name.Length < 45);
            

            foreach (var i in second)
            {
                Console.WriteLine(i);
            }
        }

        public void ThirdTask(int id, List<ProjectTask> tasks)
        {
            Console.WriteLine("---Third task---");
            Console.WriteLine();

            var third = from task in tasks
                        where task.State == 3 && task.FinishedAt.Year == 2019 && task.PerformerId == id
                        select new { task.Id, task.Name };

            foreach (var i in third)
            {
                Console.Write("id: ");
                Console.WriteLine(i.Id);
                Console.Write("name: ");
                Console.WriteLine(i.Name);
                Console.WriteLine();
            }
        }

        public void FourthTask(List<Team> teams, List<User> users)
        {
            Console.WriteLine("---Fourth task---");
            Console.WriteLine();

            var fourth = from team in teams
                         join user in users on team.Id equals user.TeamId
                         group user by team into gUbT
                         select new
                         {
                             gUbT.Key.Id,
                             gUbT.Key.Name,
                             users = gUbT.Where(u => DateTime.Now.Year - u.Birthday.Year > 12).OrderByDescending(u => u.RegistredAt)
                         };

            foreach (var i in fourth)
            {
                Console.Write("Team id: ");
                Console.WriteLine(i.Id);
                Console.Write("Name: ");
                Console.WriteLine(i.Name);
                Console.WriteLine("List of users: ");
                foreach (var j in i.users)
                {
                    Console.WriteLine(j);
                }
            }
        }

        public void FifthTask(List<User> users, List<ProjectTask> tasks)
        {
            Console.WriteLine("---Fifth task---");
            Console.WriteLine();

            var fifth = from user in users
                        join task in tasks on user.Id equals task.PerformerId
                        group task by user into gTbU
                        orderby gTbU.Key.FirstName
                        select new { gTbU.Key.FirstName, tasks = gTbU.OrderByDescending(t => t.Name.Length) };
            foreach (var i in fifth)
            {
                Console.Write("user name: ");
                Console.WriteLine(i.FirstName);
                Console.WriteLine("tasks: ");
                foreach (var j in i.tasks)
                {
                    Console.WriteLine(j);
                }
            }
        }

        public void SixthTask(int id, List<Project> projects, List<ProjectTask> tasks, List<User> users)
        {
            Console.WriteLine("---Sixth task---");
            Console.WriteLine();

            var sixth = from project in projects
                        join task in tasks on project.Id equals task.ProjectId
                        join user in users on task.PerformerId equals user.Id
                        where user.Id == id
                        group project by user into gPbU
                        select new
                        {
                            user = gPbU.Key,
                            lastProject = gPbU.OrderBy(p => p.CreatedAt).Last(),
                            lPcountTasks = tasks.Where(t => t.ProjectId == gPbU.OrderBy(p => p.CreatedAt).Last().Id).Count(),
                            notFinishedTasks = tasks.Where(t => t.PerformerId == gPbU.Key.Id && t.State != 3).Count(),
                            theLongestTask = tasks.OrderBy(t => t.FinishedAt - t.CreatedAt).Where(t => t.PerformerId == id).Last()
                        };

            foreach (var i in sixth)
            {
                Console.WriteLine("User: ");
                Console.WriteLine(i.user);
                Console.WriteLine("Last project: ");
                Console.WriteLine(i.lastProject);
                Console.Write("Count of tasks in the last project: ");
                Console.WriteLine(i.lPcountTasks);
                Console.WriteLine();
                Console.Write("Count cenceled or not finished tasks: ");
                Console.WriteLine(i.notFinishedTasks);
                Console.WriteLine("The longest task: ");
                Console.WriteLine(i.theLongestTask);
            }
        }

        public void SeventhTask(int id, List<Project> projects, List<ProjectTask> tasks, List<User> users)
        {
            Console.WriteLine("---Seventh task---");
            Console.WriteLine();

            var seventh = from project in projects
                          join task in tasks on project.Id equals task.ProjectId
                          where project.Id == id
                          group task by project into gTbP
                          select new
                          {
                              project = gTbP.Key,
                              theLngTaskByDesc = gTbP.OrderBy(t => t.Description.Length).Last(),
                              theShrtTaskByName = gTbP.OrderBy(t => t.Name.Length).First(),
                              countOfUsersInTeam = users.Where(u => u.TeamId == gTbP.Key.TeamId &&
                                    (gTbP.Key.Description.Length > 25 || tasks.Where(t => t.ProjectId == gTbP.Key.Id).Count() < 3)).Count()
                          };
            foreach (var i in seventh)
            {
                Console.WriteLine("Project: ");
                Console.WriteLine(i.project);
                Console.WriteLine("The longest task by description: ");
                Console.WriteLine(i.theLngTaskByDesc);
                Console.WriteLine("The shortest task by name: ");
                Console.WriteLine(i.theShrtTaskByName);
                Console.Write("Count of users in team: ");
                Console.WriteLine(i.countOfUsersInTeam);
            }
        }
    }
}
