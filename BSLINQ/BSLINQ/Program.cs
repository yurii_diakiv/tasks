﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.SignalR.Client;
using BSLINQ.Entities;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BSLINQ
{

    class Program
    {
        static async Task StartConnection()
        {
            HubConnection connection = new HubConnectionBuilder()
                   .WithUrl("http://localhost:58511/linqhub")
                   .Build();
            await connection.StartAsync();
            connection.On<string>("GetNotification", value => Console.WriteLine(value));
        }

        static async Task<List<T>> GetData<T>(string url)
        {
            using (var webClient = new WebClient())
            {
                string responce = await webClient.DownloadStringTaskAsync(url);
                return JsonConvert.DeserializeObject<List<T>>(responce);
            }
        }
        static Task<int> MarkRandomTaskWithDelay()
        {
            var tcs = new TaskCompletionSource<int>();

            Thread.Sleep(1000);

            MakeRTask();

            async void MakeRTask()
            {
                List<ProjectTask> tasks = await GetData<ProjectTask>("http://localhost:58511/api/tasks");
                Random random = new Random();
                var ids = (from t in tasks
                           select t.Id).ToList();
                var ind = random.Next(ids.Count);
                var idToMark = ids[ind];

                using (var client = new HttpClient())
                {
                    string x = await client.GetStringAsync("http://localhost:58511/api/tasks/" + idToMark);
                    ProjectTask task = JsonConvert.DeserializeObject<ProjectTask>(x);
                    task.State = 2;
                    var response = client.PutAsJsonAsync("http://localhost:58511/api/tasks", task).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        tcs.SetResult(idToMark);
                    }
                    else
                    {
                        throw new Exception("Error!");
                    }
                }
            }
            return tcs.Task;
        }


        static async Task Main(string[] args)
        {
            List<Project> projects = await GetData<Project>("http://localhost:58511/api/projects");
            int x = await MarkRandomTaskWithDelay();
            Console.WriteLine(x);
            List<ProjectTask> tasks = await GetData<ProjectTask>("http://localhost:58511/api/tasks");
            x = await MarkRandomTaskWithDelay();
            Console.WriteLine(x);
            List<TaskState> taskStates = await GetData<TaskState>("http://localhost:58511/api/taskstates");
            x = await MarkRandomTaskWithDelay();
            Console.WriteLine(x);
            List<Team> teams = await GetData<Team>("http://localhost:58511/api/teams");
            x = await MarkRandomTaskWithDelay();
            Console.WriteLine(x);
            List<User> users = await GetData<User>("http://localhost:58511/api/users");
            x = await MarkRandomTaskWithDelay();
            Console.WriteLine(x);

            LINQRequests requests = new LINQRequests();

            requests.FirstTask(2, projects, tasks);
            requests.SecondTask(4, tasks);
            requests.ThirdTask(5, tasks);
            requests.FourthTask(teams, users);
            requests.FifthTask(users, tasks);
            requests.SixthTask(2, projects, tasks, users);
            requests.SeventhTask(3, projects, tasks, users);
        }
    }
}