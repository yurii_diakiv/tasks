﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quas ipsa magnam ratione.Culpa iusto non quisquam voluptates.", new DateTime(2019, 11, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dolorum ullam nesciunt consequuntur quis modi mollitia aliquid doloremque.", 21, 11, 1 },
                    { 2, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Doloribus ab omnis expedita aut sit sunt animi", new DateTime(2019, 2, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nemo quo voluptates eius eum.", 22, 12, 2 },
                    { 3, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nisi et aut. Eius natus magni ratione totam. Nulla tempore incidunt itaque rem.", new DateTime(2019, 8, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Omnis et et.", 23, 13, 3 },
                    { 4, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Earum vel hic rerum sequi. Nostrum unde molestiae voluptate dolores eius deserunt nobis.", new DateTime(2019, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Veniam voluptatem velit in rem aut ut aut quis.", 24, 14, 4 },
                    { 5, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aut libero deserunt ut assumenda quo et ducimus sequi sunt. Deleniti voluptas omnis enim iure.", new DateTime(2019, 4, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Voluptatem praesentium eos in.", 25, 15, 5 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 37, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Consectetur nisi voluptate vel earum inventore", "Odit eveniet libero voluptatibus numquam.", 5 },
                    { 2, 45, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ut magnam aliquid quo dolores id", "Illo et et molestiae aut.", 2 },
                    { 3, 1, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 9, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Veniam vitae harum rerum aspernatur iure", "Eius fugiat amet molestias eos.", 4 },
                    { 4, 27, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Minima laboriosam provident esse quibusdam quibusdam reprehenderit soluta", "Aliquid qui sed possimus quidem.", 9 },
                    { 5, 4, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et nihil deserunt. Voluptatum velit maxime eos reiciendis laborum", "Iste est quae beatae et.", 6 }
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 4, "Canceled" },
                    { 3, "Finished" },
                    { 2, "Started" },
                    { 1, "Created" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "labore" },
                    { 2, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "reprehenderit" },
                    { 3, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "qui" },
                    { 4, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "repudiandae" },
                    { 5, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "nobis" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegistredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2002, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Letha.Strosin58@gmail.com", "Letha", "Strosin", new DateTime(2019, 6, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 4 },
                    { 2, new DateTime(2010, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jeanette.Shields@yahoo.com", "Jeanette", "Shields", new DateTime(2019, 5, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 8 },
                    { 3, new DateTime(2008, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wilbert95@hotmail.com", "Wilbert", "Hintz", new DateTime(2019, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 7 },
                    { 4, new DateTime(2006, 7, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Danyka_Bernier@yahoo.com", "Danyka", "Bernier", new DateTime(2019, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 },
                    { 5, new DateTime(2008, 1, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kurt65@gmail.com", "Kurt", "Oberbrunner", new DateTime(2019, 6, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 5 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
