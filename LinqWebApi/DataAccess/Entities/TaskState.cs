﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public TaskState(int id, string value)
        {
            Id = id;
            Value = value;
        }
    }
}
