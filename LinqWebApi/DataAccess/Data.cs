﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Entities;

namespace DataAccess
{
    public class Data
    {
        public List<Project> GetProjects()
        {
            List<Project> projects = new List<Project>();

            Project project1 = new Project
            (
                1,
                "Odit eveniet libero voluptatibus numquam.",
                "Consectetur nisi voluptate vel earum inventore",
                new DateTime(2019, 06, 19),
                new DateTime(2020, 05, 07),
                37,
                5
            );

            Project project2 = new Project
            (
                2,
                "Illo et et molestiae aut.",
                "Ut magnam aliquid quo dolores id",
                new DateTime(2019, 06, 18),
                new DateTime(2019, 07, 26),
                45,
                2
            );

            Project project3 = new Project
            (
                3,
                "Eius fugiat amet molestias eos.",
                "Veniam vitae harum rerum aspernatur iure",
                new DateTime(2019, 06, 18),
                new DateTime(2019, 09, 02),
                1,
                4
            );

            Project project4 = new Project
            (
                4,
                "Aliquid qui sed possimus quidem.",
                "Minima laboriosam provident esse quibusdam quibusdam reprehenderit soluta",
                new DateTime(2019, 06, 18),
                new DateTime(2020, 04, 19),
                27,
                9
            );

            Project project5 = new Project
            (
                5,
                "Iste est quae beatae et.",
                "Et nihil deserunt. Voluptatum velit maxime eos reiciendis laborum",
                new DateTime(2019, 06, 18),
                new DateTime(2020, 06, 01),
                4,
                6
            );

            projects.Add(project1);
            projects.Add(project2);
            projects.Add(project3);
            projects.Add(project4);
            projects.Add(project5);

            return projects;
        }

        public List<ProjectTask> GetTasks()
        {
            List<ProjectTask> tasks = new List<ProjectTask>();

            ProjectTask task1 = new ProjectTask
            (
                1,
                "Dolorum ullam nesciunt consequuntur quis modi mollitia aliquid doloremque.",
                "Quas ipsa magnam ratione.Culpa iusto non quisquam voluptates.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 11, 30),
                1,
                11,
                21
            );

            ProjectTask task2 = new ProjectTask
            (
                2,
                "Nemo quo voluptates eius eum.",
                "Doloribus ab omnis expedita aut sit sunt animi",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 02, 05),
                2,
                12,
                22
            );

            ProjectTask task3 = new ProjectTask
            (
                3,
                "Omnis et et.",
                "Nisi et aut. Eius natus magni ratione totam. Nulla tempore incidunt itaque rem.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 08, 18),
                3,
                13,
                23
            );

            ProjectTask task4 = new ProjectTask
            (
                4,
                "Veniam voluptatem velit in rem aut ut aut quis.",
                "Earum vel hic rerum sequi. Nostrum unde molestiae voluptate dolores eius deserunt nobis.",
                new DateTime(2019, 06, 18),
                new DateTime(2019, 11, 01),
                4,
                14,
                24
            );

            ProjectTask task5 = new ProjectTask
            (
                5,
                "Voluptatem praesentium eos in.",
                "Aut libero deserunt ut assumenda quo et ducimus sequi sunt. Deleniti voluptas omnis enim iure.",
                new DateTime(2019, 06, 19),
                new DateTime(2019, 04, 12),
                5,
                15,
                25
            );

            tasks.Add(task1);
            tasks.Add(task2);
            tasks.Add(task3);
            tasks.Add(task4);
            tasks.Add(task5);

            return tasks;
        }

        public List<TaskState> GetTaskStates()
        {
            List<TaskState> taskStates = new List<TaskState>();

            TaskState taskState1 = new TaskState(1, "Created");
            TaskState taskState2 = new TaskState(2, "Started");
            TaskState taskState3 = new TaskState(3, "Finished");
            TaskState taskState4 = new TaskState(4, "Canceled");

            taskStates.Add(taskState1);
            taskStates.Add(taskState2);
            taskStates.Add(taskState3);
            taskStates.Add(taskState4);

            return taskStates;
        }

        public List<Team> GetTeams()
        {
            List<Team> teams = new List<Team>();

            Team team1 = new Team(1, "labore", new DateTime(2019, 06, 19));
            Team team2 = new Team(2, "reprehenderit", new DateTime(2019, 06, 19));
            Team team3 = new Team(3, "qui", new DateTime(2019, 06, 18));
            Team team4 = new Team(4, "repudiandae", new DateTime(2019, 06, 18));
            Team team5 = new Team(5, "nobis", new DateTime(2019, 06, 19));

            teams.Add(team1);
            teams.Add(team2);
            teams.Add(team3);
            teams.Add(team4);
            teams.Add(team5);

            return teams;
        }

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();

            User user1 = new User
            (
                1,
                "Letha",
                "Strosin",
                "Letha.Strosin58@gmail.com",
                new DateTime(2002, 05, 11),
                new DateTime(2019, 06, 17),
                4
            );

            User user2 = new User
            (
                2,
                "Jeanette",
                "Shields",
                "Jeanette.Shields@yahoo.com",
                new DateTime(2010, 02, 11),
                new DateTime(2019, 05, 17),
                8
            );

            User user3 = new User
            (
                3,
                "Wilbert",
                "Hintz",
                "Wilbert95@hotmail.com",
                new DateTime(2008, 05, 06),
                new DateTime(2019, 05, 11),
                7
            );

            User user4 = new User
            (
                4,
                "Danyka",
                "Bernier",
                "Danyka_Bernier@yahoo.com",
                new DateTime(2006, 07, 08),
                new DateTime(2019, 05, 31),
                3
            );

            User user5 = new User
            (
                5,
                "Kurt",
                "Oberbrunner",
                "Kurt65@gmail.com",
                new DateTime(2008, 01, 16),
                new DateTime(2019, 06, 11),
                5
            );

            users.Add(user1);
            users.Add(user2);
            users.Add(user3);
            users.Add(user4);
            users.Add(user5);

            return users;
        }
    }
}
