﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LinqWebApi.Models
{
    public class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        public Team(int id, string name, DateTime createdAt)
        {
            Id = id;
            Name = name;
            CreatedAt = createdAt;
        }



        //public int id { get; set; }
        //public string name { get; set; }
        //public DateTime created_at { get; set; }

        //public override string ToString()
        //{
        //    return $"id : {id}\nname : {name}\ncreated_at : {created_at}\n";
        //}
    }
}
