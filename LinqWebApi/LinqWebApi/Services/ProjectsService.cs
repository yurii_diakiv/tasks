﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqWebApi.Interfaces;
using DataAccess.Entities;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IRepository<Project> repository;

        public ProjectsService(ProjectRepository projectRepository)
        {
            repository = projectRepository;
        }
        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await repository.GetItems();
        }

        public async Task<Project> GetProject(int id)
        {
            return await repository.GetItem(id);
        }

        public async Task Create(Project item)
        {
            await repository.Create(item);
        }

        public async Task Update(Project project)
        {
            await repository.Update(project);
        }

        public async Task Delete(int id)
        {
            await repository.Delete(id);
        }
    }
}
