﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Microsoft.Extensions.Configuration;
using QueueServices.Services;
using QueueServices.Interfaces;
using QueueServices.Models;
using Microsoft.AspNetCore.SignalR;
using LinqWebApi.Hubs;

namespace LinqWebApi.Services
{
    public class QueueService// : IQueueService
    {
        private readonly /*IMessageProducerScope*/MessageProducerScope messageProducerScope;
        private readonly /*IMessageConsumerScope*/MessageConsumerScope messageConsumerScope;
        private readonly IHubContext<ServerHub> serverHub;

        public QueueService(
            /*IMessageProducerScopeFactory*/MessageProducerScopeFactory messageProducerScopeFactory,
            /*IMessageConsumerScopeFactory*/MessageConsumerScopeFactory messageConsumerScopeFactory,
            IHubContext<ServerHub> hubContext)
        {
            serverHub = hubContext;

            messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue,",
                RoutingKey = "topic.queue"
            });

            messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });

            messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public bool PostValue(string value)
        {
            try
            {
                messageProducerScope.MessageProducer.Send(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void GetValue(object sender, BasicDeliverEventArgs args)
        {
            var value = Encoding.UTF8.GetString(args.Body);
            serverHub.Clients.All.SendAsync(method: "GetNotification", value);
            messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed: true);
        }
    }
}
