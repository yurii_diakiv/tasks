﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IRepository<Team> repository;

        public TeamsService(TeamRepository teamRepository)
        {
            repository = teamRepository;
        }
        public async Task<IEnumerable<Team>> GetTeams()
        {
            return await repository.GetItems();
        }

        public async Task<Team> GetTeam(int id)
        {
            return await repository.GetItem(id);
        }

        public async Task Create(Team item)
        {
            await repository.Create(item);
        }

        public async Task Update(Team team)
        {
            await repository.Update(team);
        }

        public async Task Delete(int id)
        {
            await repository.Delete(id);
        }
    }
}
