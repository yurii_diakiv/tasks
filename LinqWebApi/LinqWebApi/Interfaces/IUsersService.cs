﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    interface IUsersService
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id);
        Task Create(User item);
        Task Update(User item);
        Task Delete(int id);
    }
}
