﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    interface ITeamsService
    {
        Task<IEnumerable<Team>> GetTeams();
        Task<Team> GetTeam(int id);
        Task Create(Team item);
        Task Update(Team item);
        Task Delete(int id);

    }
}
