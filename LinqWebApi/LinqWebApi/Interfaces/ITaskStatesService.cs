﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    interface ITaskStatesService
    {
        Task<IEnumerable<TaskState>> GetTaskStates();
        Task<TaskState> GetTaskState(int id);
        Task Create(TaskState item);
        Task Update(TaskState item);
        Task Delete(int id);
    }
}
