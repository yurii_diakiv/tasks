﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class TaskStateRepository : IRepository<TaskState>
    {
        private readonly ProjectDbContext dbContext;

        public TaskStateRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public async Task<IEnumerable<TaskState>> GetItems()
        {
            return await dbContext.Set<TaskState>().ToListAsync();
        }

        public async Task<TaskState> GetItem(int id)
        {
            return await dbContext.Set<TaskState>().FindAsync(id);
        }

        public async Task Create(TaskState p)
        {
            await dbContext.Set<TaskState>().AddAsync(p);
            await dbContext.SaveChangesAsync();
        }

        public async Task Update(TaskState p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var p = await dbContext.Set<TaskState>().FindAsync(id);
            dbContext.Set<TaskState>().Remove(p);
            await dbContext.SaveChangesAsync();
        }
    }
}
