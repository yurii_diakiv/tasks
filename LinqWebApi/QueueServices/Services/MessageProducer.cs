﻿using System;
using System.Collections.Generic;
using System.Text;
using QueueServices.Interfaces;
using RabbitMQ.Client;
using QueueServices.Models;

namespace QueueServices.Services
{
    public class MessageProducer// : IMessageProducer
    {
        private readonly MessageProducerSettings messageProducerSettings;
        private readonly IBasicProperties properties;

        public MessageProducer(MessageProducerSettings messageProducerSett)
        {
            messageProducerSettings = messageProducerSett;
            properties = messageProducerSett.Channel.CreateBasicProperties();
            properties.Persistent = true;
        }

        public void Send(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
            {
                properties.Type = type;
            }
            var body = Encoding.UTF8.GetBytes(message);
            messageProducerSettings.Channel.BasicPublish(messageProducerSettings.PublicationAddress, properties, body);
        }

        public void SendTyped(Type type, string message)
        {
            Send(message, type.AssemblyQualifiedName);
        }
    }
}
